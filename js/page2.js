// 1.
$('#btn01').on('click', function () {
  $('p:even').css("backgroundColor", "green")
})

$('#btn02').on('click', function () {
  var pars = $('p');
  var random = Math.floor(Math.random() * pars.length);
  pars.eq(random).css('visibility', 'hidden');
  console.log(random);
})

$('#btn03').on('click', function () {
  var par1 = $('p').eq(0);
  var par4 = $('p').eq(3);
  var p1text = par1.text();
  var p4text = par4.text();
  par1.text(p4text);
  par4.text(p1text);
})

$('#btn04').on('click', function () {
  var pars = $('p');
  for (var index = 0; index < pars.length; index++) {
    var par = pars.eq(index).text();
    par = par.toUpperCase();
    $('p').eq(index).text(par);
  }
})


// 2.
$('#btn05').on('click', function () {
  $('#mytable tr:even').css('backgroundColor', 'yellow')
  $('#mytable tr:odd').css('backgroundColor', 'cyan')
  $('#mytable tr').eq(0).css('backgroundColor', 'grey')
})

$('#btn06').on('click', function () {
  $('#mytable').append('<tr><td>Meaningful Content</td><td>Is it though?</td><td>Definitely</td></tr>');
  $('#mytable').prepend('<tr><td>Additional Content</td><td>Much So</td><td>Always</td></tr>')
})

$('#btn07').on('click', function () {
  var pics = ['again.jpg', 'air.jpg', 'ball.jpg', 'blue.jpg', 'branch.jpg'];
  var random = Math.floor(Math.random() * pics.length);
  $('#mytable tr:last').html('<img src="images/' + pics[random] + '">');
})

// $('#btn08').on('click', function () {
//     $('#mytable tr').each(function () {
//         $('td:eq(1)').html("");
//     })
// })

$('#btn08').on('click', function () {
  var mytable = $('#mytable');
  for (var line = 0; line < mytable[0].rows.length; line++) {
    var myrow = mytable[0].rows[line];
    for (var column = 0; column < myrow.cells.length; column++) {
      var mycell = myrow.cells[column];
      if (mycell.innerHTML == "Ottawa") {
        mycell.innerHTML = "";
      }
      if (mycell.innerHTML == "Paris") {
        mycell.innerHTML = "";
      }
      if (mycell.innerHTML == "May I Guess") {
        mycell.innerHTML = "";
      }
      if (mycell.innerHTML == " Could it be ?") {
        mycell.innerHTML = "";
      }
    }
  }
})


// 3.
$('#btn09').on('click', function () {
  var images = $('img');
  for (var count = 0; count < images.length; count++) {
    var alt = images.eq(count).attr('alt');
    console.log(alt);
    $('p').eq(count + 5).text(alt)
  }
  $('p').eq(4).text("")
})

$('#btn10').on('click', function () {
  var par2 = $('#paragraph-1-2');
  var pics = ['again.jpg', 'air.jpg', 'ball.jpg', 'blue.jpg', 'branch.jpg'];
  var random = Math.floor(Math.random() * pics.length);
  par2.replaceWith("<img src='images/" + pics[random] + "' id='paragraph-1-2'>")
})

$('#btn11').on('click', function () {
  var imageWidth = $('img:first').attr("width");
  imageWidth = parseInt(imageWidth);
  var $myImage = $('img:first');
  var myWidth = $myImage.attr("width");
  myWidth = parseInt(myWidth);
  if (myWidth === imageWidth) {
    $myImage.attr("width", imageWidth / 2);
    $myImage.attr("height", "auto");
  }
})

const originalWidth = $('img:first').attr("width");
$('#btn12').on('click', function () {
  var variableWidth = $('img:first').attr("width");
  variableWidth = parseInt(variableWidth);
  var images = $('img');
  if (variableWidth == originalWidth) {
    for (var count = 0; count < images.length; count++) {
      images.eq(count).attr("width", originalWidth / 2);
      images.eq(count).attr("height", "auto");
      console.log(count);
    }
    variableWidth = (originalWidth / 2);
  } else {
    for (var count = 0; count < images.length; count++) {
      images.eq(count).attr("width", originalWidth);
      images.eq(count).attr("height", "auto");
      variableWidth = (originalWidth / 2);
    }
    variableWidth = originalWidth;
  }
})


// 4.
$('#btn13').on('click', function () {
  $('li').each(function () {
    var pics = ['again.jpg', 'air.jpg', 'ball.jpg', 'blue.jpg', 'branch.jpg', 'capsule.jpg'];
    var random = Math.floor(Math.random() * pics.length);
    $(this).append("<br><img src='images/" + pics[random] + "' width='70px' height='auto'>")
  })
})

$('#btn14').on('click', function () {
  var lis = $('li');
  for (var count = 0; count < lis.length; count++) {
    var liFontSize = $('li').eq(count).css("fontSize");
    liFontSize = parseInt(liFontSize);
    var newFontSize = (liFontSize * 1.25);
    $('li').eq(count).css("fontSize", newFontSize + "px")
  }
})

$('#btn15').on('click', function () {
  var ulList = $('#ulist');
  var olList = $('#olist');
  ulListHTML = ulList.html();
  olListHTML = olList.html();
  // var tmp = ulListHTML;
  // ulListHTML = olListHTML;
  // olListHTML = tmp;
  ulList.html(olListHTML);
  olList.html(ulListHTML);
})

$('#btn16').on('click', function () {
  var list = $('#ulist li');
  var tmp = $('#ulist li:last').html();
  for (var count = 0; count < list.length; count++) {
    var spot1 = $('#ulist li').eq(count).html();
    $('#ulist li').eq(count).html(tmp);
    tmp = $('#ulist li').eq(count + 1).html();
    console.log("spot1: " + spot1);
    console.log("tmp: " + tmp);
    $('#ulist li').eq(count + 1).html(spot1);
    count++;
  }
})