// 1.
var image = $("img:first");

var mywidth = $("body").innerWidth() - $("img:first").width();
var leftMargin = "400";
var widthMinusMargin = (mywidth - leftMargin);

function moveRight() {
  if (stop == false) {
    $("img:first").css("position", "relative");
    $("img:first").animate({
      left: widthMinusMargin + "px"
    }, 2000);
    leftSide = false;
    setTimeout(moveLeft, 2050);
  }
}

function moveLeft() {
  if (stop == false) {
    $("img:first").css("position", "relative");
    $("img:first").animate({
      left: 0 + "px"
    }, 2000);
    leftSide = true;
    setTimeout(moveRight, 2050);
  }
}

var leftSide = true;
var stop = false;
moveRight();
//moveLeft();
$("#btn09").on("click", function () {
  if (stop == false) {
    stop = true; // stop movement
  } else {
    // restart movement
    stop = false;
    if (leftSide == true) moveRight();
    else moveLeft();
  }
});



// 2.
var index = 0
var ok = true;

function animateImage() {
  var gallery = $('img:last');
  var pics = ['again.jpg', 'air.jpg', 'ball.jpg', 'blue.jpg', 'branch.jpg', 'capsule.jpg'];
  var random = Math.floor(Math.random() * pics.length);
  gallery.html("<img src='images/'" + pics[random] + "'>");
  gallery.css("border", "3px solid blue");
  gallery.replaceWith("<img src='images/" + pics[random] + "'>")
}

if (ok) {
  while(index < 100)
  {
    setTimeout(animateImage(), 2500);
    index++;
  }
}
