// 1.
$('#btn01').on('click', function () {
  $('p:lt(4)').hide(3500, function () {
    $('p:eq(3)').css('display', 'none');
    $('p:eq(4)').css('display', 'none');
    $('p:lt(2)').show(3500);
  });
});


// 2.
for (var interval = 0; interval < 1; interval++) {
  $('h3:eq(0)').on('mouseenter', function () {
    $('h3:eq(0)').slideToggle(500)
  });
  $('h3:eq(0)').on('mouseleave', function () {
    $('h3:eq(0)').slideToggle(500)
  });
}


// 3.
$('table').on('click', function () {
  $('table').animate({
    "fontSize": "24px"
  }, 2000)
})
$('table').on('mouseleave', function () {
  $('table').animate({
    "fontSize": "12px"
  }, 3000)
})


// 4.
$('#btn09').on('click', function () {
  $('img:lt(2)').slideUp(3000)

  var img1 = $('img:eq(0)').attr('src')
  var img2 = $('img:eq(1)').attr('src')
  $('img:eq(0)').attr('src', img2)
  $('img:eq(1)').attr('src', img1)

  $('img:lt(2)').slideDown(2500)
})